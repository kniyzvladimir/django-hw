import random
from uuid import uuid4

from faker import Faker

from teachers2.models import Teacher


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)


def padding_fake_data_teachers(count):
    for st in range(count):
        fake = Faker()
        teacher = Teacher(
            uuid=uuid4(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            birthdate=fake.date_time(),
            rating=random.randint(0, 100),
            certification_date=fake.date(),
            email=fake.email()
        )
        teacher.save()
