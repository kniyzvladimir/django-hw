from uuid import uuid4

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from students.utils import get_count

from teachers2.forms import TeacherCreateForm
from teachers2.models import Teacher
from teachers2.utils import padding_fake_data_teachers


def get_teachers(request):
    teachers = Teacher.objects.all()
    params = [
        "first_name",
        "last_name",
        "rating"
    ]
    for param in params:
        value = request.GET.get(param)
        if value:
            teachers = teachers.filter(**{param: value})
    return render(
        request=request,
        template_name='teachers-list.html',
        context={
            'teachers': teachers
        }
    )


def generate_teachers(request):
    try:
        count = get_count(request)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)
    padding_fake_data_teachers(count)
    return HttpResponse("Ok")


def create_teacher(request):
    if request.method == 'GET':
        form = TeacherCreateForm()

    elif request.method == 'POST':
        form = TeacherCreateForm(request.POST)
        if form.is_valid():
            form.save()
            th = Teacher.objects.last()
            th.uuid = uuid4()
            th.save()
            return HttpResponseRedirect(reverse('teachers:list'))

    return render(
        request=request,
        template_name='teachers-create.html',
        context={
            'form': form
        }
    )


def edit_teacher(request, uuid):
    try:
        teacher = Teacher.objects.get(uuid=uuid)
    except Teacher.DoesNotExist:
        return HttpResponse("Student doesn`t exist", status=404)

    if request.method == 'GET':
        form = TeacherCreateForm(instance=teacher)

    elif request.method == 'POST':
        form = TeacherCreateForm(
            data=request.POST,
            instance=teacher
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:list'))

    return render(
        request=request,
        template_name='teachers-edit.html',
        context={
            'form': form,
            'teacher': teacher,
            'groups': teacher.groups.all()
        }
    )


def delete_techear(request, uuid):
    teacher = get_object_or_404(Teacher, uuid=uuid)
    teacher.delete()
    return HttpResponseRedirect(reverse('teachers:list'))
