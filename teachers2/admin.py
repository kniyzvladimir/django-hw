from django.contrib import admin # noqa

from groups.models import Group

from teachers2.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    fields = ['first_name', 'course']
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'birthdate', 'rating', 'certification_date', 'email']
    exclude = ['uuid']
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
