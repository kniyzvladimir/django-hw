import datetime

from core.models import Persona

from django.db import models


class Teacher(Persona):
    # uuid = models.TextField(null=False)
    # first_name = models.CharField(max_length=64, null=False)
    # last_name = models.CharField(max_length=84, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)
    rating = models.SmallIntegerField(null=True, default=0)
    certification_date = models.DateField(null=True, default=datetime.date.today)
    email = models.EmailField(null=True, max_length=128)

    # def full_name(self):
    #     return f'{self.first_name}, {self.last_name}'

    def other_information(self):
        return f'{self.rating}, {self.certification_date}, {self.email}'

    def __str__(self):
        return f'{self.id}, {super().__str__()}, {self.other_information()}'
