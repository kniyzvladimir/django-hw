from django.urls import path

from teachers2.views import create_teacher, delete_techear, edit_teacher, generate_teachers, get_teachers

app_name = 'teachers'

urlpatterns = [
    path('generate/', generate_teachers, name='generate'),
    path('', get_teachers, name='list'),
    path('create/', create_teacher, name='create'),
    path('edit/<uuid:uuid>', edit_teacher, name='edit'),
    path('delete/<uuid:uuid>', delete_techear, name='delete'),
]
