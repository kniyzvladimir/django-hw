from course.models import Course

from django import forms


class CourseCreateForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['course_name']
