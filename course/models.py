import uuid as uuid

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Technology(models.Model):
    technology_name = models.TextField(null=False)
    lessons_quantity = models.PositiveSmallIntegerField(default=12,
                                                        validators=[MinValueValidator(6), MaxValueValidator(48)])
    know_english = models.BooleanField(default=True)
    other_information = models.TextField(null=True, max_length=400)
    uuid = models.UUIDField(default=uuid.uuid4)

    def __str__(self):
        return f'{self.technology_name}, {self.lessons_quantity}'


class Course(models.Model):
    course_name = models.TextField(null=False)
    students_quantity = models.PositiveSmallIntegerField(default=30,
                                                         validators=[MinValueValidator(5), MaxValueValidator(40)])
    online_learning = models.BooleanField(default=True)
    stipend = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4)
    technologies = models.ManyToManyField(
        to=Technology,
        related_name='course'
    )

    def __str__(self):
        return f'{self.course_name}'
