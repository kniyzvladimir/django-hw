from course.models import Course, Technology

from django.contrib import admin


class CourseAdmin(admin.ModelAdmin):
    exclude = ['uuid']
    list_display = ['course_name', 'students_quantity', 'online_learning', 'stipend']


class TechnologyAdmin(admin.ModelAdmin):
    exclude = ['uuid']
    list_display = ['technology_name', 'lessons_quantity', 'know_english']


admin.site.register(Course, CourseAdmin)
admin.site.register(Technology, TechnologyAdmin)
