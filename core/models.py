import datetime

from django.db import models # noqa


class BaseModel(models.Model):
    class Meta:
        abstract = True

    create_date = models.DateField(auto_now_add=True, null=True)
    write_date = models.DateField(auto_now_add=True, null=True)

    def save(self, *args, **kwargs):
        self.write_date = datetime.date.today()
        super().save(*args, **kwargs)


class Persona(BaseModel):
    class Meta:
        abstract = True

    uuid = models.TextField(null=False)
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)

    def __str__(self):
        return f"{self.first_name}, {self.last_name}"
