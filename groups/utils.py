import random
from uuid import uuid4

from faker import Faker

from groups.models import Group

from teachers2.models import Teacher


def name_course():
    n = str(random.randint(1, 9))
    name_cours = 'Group - ' + n
    return name_cours


course_list = ['Ни то ни сё', 'Есть надежда', 'Троешники', 'Бездельники',
               'Двоешники', 'Стараются', 'Движутся к успеху', 'Гении']


def padding_fake_data_groups(count):
    for st in range(count):
        fake = Faker()
        group = Group(
            uuid=uuid4(),
            first_name=name_course(),
            birthdate=fake.date_time(),
            teacher=random.choice(Teacher.objects.all()),
            course=random.choice(course_list)
        )
        group.save()
