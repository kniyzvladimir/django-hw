from django.contrib import admin

from groups.models import Classroom, Group

from students.models import Students


class StudentTable(admin.TabularInline):
    model = Students
    fields = ['first_name', 'last_name', 'rating']
    readonly_fields = fields
    show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'course', 'head']
    fields = ['first_name', 'course', 'classrooms', 'teacher', 'head']
    inlines = [StudentTable]
    list_select_related = ['head']


admin.site.register(Classroom)
admin.site.register(Group, GroupAdmin)
