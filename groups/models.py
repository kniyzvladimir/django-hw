import datetime

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from teachers2.models import Teacher


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f'{self.name}, {self.floor}'


class Group(models.Model):
    uuid = models.TextField(null=False)
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, default='---')
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)
    course = models.CharField(max_length=84, null=False)
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )
    teacher = models.ForeignKey(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    head = models.OneToOneField(
        to='students.Students',
        null=True,
        on_delete=models.SET_NULL,
        related_name='head_of_group'
    )

    def __str__(self):
        return f'{self.id}{self.first_name}, {self.course}'

    def clean(self):
        gr = Group.objects.get(id=self.id)
        group_students = gr.students.all()
        if self.head not in group_students:
            raise ValidationError("Студент не входи в состав группы")

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)
