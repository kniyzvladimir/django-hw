from django.urls import path

from groups.views import create_group, delete_group, edit_group, generate_groups, get_groups

app_name = 'groups'

urlpatterns = [
    path('create/', create_group, name='create'),
    path('delete/<uuid:uuid>', delete_group, name='delete'),
    path('generate/', generate_groups, name='generate'),
    path('edit/<uuid:uuid>', edit_group, name='edit'),
    path('', get_groups, name='list'),

]
