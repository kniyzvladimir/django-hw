from uuid import uuid4

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from groups.forms import GroupsCreateForm
from groups.models import Group
from groups.utils import padding_fake_data_groups

from students.utils import get_count


def get_groups(request):
    groups = Group.objects.select_related('teacher').all()
    first_name = request.GET.get('first_name')
    course = request.GET.get('course')

    if first_name:
        groups = groups.filter(first_name=first_name)
    if course:
        groups = groups.filter(last_name=course)

    return render(
            request=request,
            template_name='groups-list.html',
            context={
                'groups': groups
            }
        )


def generate_groups(request):
    try:
        count = get_count(request)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)
    padding_fake_data_groups(count)
    return HttpResponseRedirect(reverse('groups:list'))


def create_group(request):
    if request.method == 'GET':
        form = GroupsCreateForm()
    elif request.method == 'POST':
        form = GroupsCreateForm(request.POST)
        if form.is_valid():
            form.save()
            gr = Group.objects.last()
            gr.uuid = uuid4()
            gr.save()
            return HttpResponseRedirect(reverse('groups:list'))
    return render(
        request=request,
        template_name='groups-create.html',
        context={
            'form': form
        }
    )


def edit_group(request, uuid):
    try:
        group = Group.objects.get(uuid=uuid)
    except Group.DoesNotExist:
        return HttpResponse("Group doesn`t exist", status=404)

    if request.method == 'GET':
        form = GroupsCreateForm(instance=group)
    elif request.method == 'POST':
        form = GroupsCreateForm(
            data=request.POST,
            instance=group
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:list'))

    return render(
        request=request,
        template_name='groups-edit.html',
        context={
            'form': form,
            'group': group,
            'students': group.students.all()
        }
    )


def delete_group(request, uuid):
    group = get_object_or_404(Group, uuid=uuid)
    group.delete()

    return HttpResponseRedirect(reverse('groups:list'))
