# Generated by Django 3.1 on 2020-09-28 22:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0007_auto_20200928_2156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='classrooms',
            field=models.ManyToManyField(related_name='groups', to='groups.Classroom'),
        ),
    ]
