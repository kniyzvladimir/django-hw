from django import forms

from groups.models import Group


class GroupsCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['first_name', 'course', 'teacher', 'head', 'classrooms']
