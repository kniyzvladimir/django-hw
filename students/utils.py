import random
from uuid import uuid4

from faker import Faker

from groups.models import Group

from students.models import Students


fake = Faker()


def get_count(request):
    count = request.GET.get('count')

    if count is None:
        raise ValueError("ERROR: indicate the quantity")
    if not count.isdigit():
        raise ValueError("VALUE ERROR: int")

    f_count = float(count)
    count = int(count)

    if not 0 < count <= 100:
        raise ValueError("RANGE ERROR: [1..100]")
    if not f_count == count:
        raise ValueError("VALUE ERROR: whole number only")

    return count


def padding_fake_data_students(count):
    all_groups = Group.objects.all()
    for st in range(count):
        uuid = uuid4()
        fn = fake.first_name()
        ln = fake.last_name()
        bd = fake.date_time()
        rating = random.randint(0, 100)
        phone_number = ''
        for i in range(Students.phone_number.field.max_length):
            i = str(random.randint(0, 9))
            phone_number += i
        group = random.choice(all_groups)
        st = Students(uuid=uuid, first_name=fn, last_name=ln, birthdate=bd,
                      rating=rating, phone_number=phone_number, group=group)
        st.save()


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)
