from django import forms
from django.http import HttpResponse

from students.models import Students


class StudentsCreateForm(forms.ModelForm):
    class Meta:
        model = Students
        fields = ['first_name', 'last_name', 'rating', 'phone_number', 'group']

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        if phone_number is None:
            raise ValueError("ERROR: indicate the phone number")
        if not phone_number.isdigit():
            raise ValueError("VALUE ERROR: only digital values")

        try:
            return phone_number
        except Exception as ex:
            return HttpResponse(str(ex), status_code=400)
