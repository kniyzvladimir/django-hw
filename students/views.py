from uuid import uuid4

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from faker import Faker

from students.forms import StudentsCreateForm
from students.models import Students
from students.utils import get_count, padding_fake_data_students

fake = Faker()


def hello(request):
    return HttpResponse('Hello from Django')


def generate_students(request):
    try:
        count = get_count(request)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)
    padding_fake_data_students(count)

    return HttpResponseRedirect(reverse('students:list'))


def get_students(request):
    students = Students.objects.select_related('group').all()

    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    rating = request.GET.get('rating')
    phone_number = request.GET.get('phone_number')

    if first_name:
        students = students.filter(first_name=first_name)
    if last_name:
        students = students.filter(last_name=last_name)
    if rating:
        students = students.filter(rating=rating)
    if phone_number:
        students = students.filter(phone_number=phone_number)

    return render(
        request=request,
        template_name='students-list.html',
        context={
            'students': students,
        }
    )


def create_student(request):

    if request.method == 'GET':
        form = StudentsCreateForm()

    elif request.method == 'POST':
        form = StudentsCreateForm(request.POST)
        if form.is_valid():
            form.save()
            st = Students.objects.last()
            st.uuid = uuid4()
            st.save()
            return HttpResponseRedirect(reverse('students:list'))

    return render(
        request=request,
        template_name='students-create.html',
        context={
            'form': form,
        }
    )


def edit_student(request, uuid):
    try:
        student = Students.objects.get(uuid=uuid)
    except Students.DoesNotExist:
        return HttpResponse("Student doesn`t exist", status=404)

    if request.method == 'GET':
        form = StudentsCreateForm(instance=student)

    elif request.method == 'POST':
        form = StudentsCreateForm(
            data=request.POST,
            instance=student
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('students:list'))

    return render(
        request=request,
        template_name='students-edit.html',
        context={
            'form': form,
            'student': student
        }
    )


def delete_student(request, uuid):
    student = get_object_or_404(Students, uuid=uuid)
    student.delete()

    return HttpResponseRedirect(reverse('students:list'))
