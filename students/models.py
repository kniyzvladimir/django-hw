import datetime

from core.models import Persona

from django.db import models

from groups.models import Group


class Students(Persona):
    # uuid = models.TextField(null=False)
    # first_name = models.CharField(max_length=64, null=False)
    # last_name = models.CharField(max_length=84, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)
    rating = models.SmallIntegerField(null=True, default=0)
    phone_number = models.TextField(null=True, max_length=12)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    # def full_name(self):
    #     return f'{self.first_name}, {self.last_name}'

    def age(self):
        return datetime.datetime.now().date().year - self.birthdate.year

    def __str__(self):
        return f'{super().__str__()}, {self.age()}, {self.rating}, {self.phone_number}'
