from django.contrib import admin

from students.models import Students


class StudentAdmin(admin.ModelAdmin):
    exclude = ['uuid']
    readonly_fields = ['phone_number']


admin.site.register(Students, StudentAdmin)
