from django.urls import path

from students.views import create_student, delete_student, edit_student, generate_students, get_students

app_name = 'students'

urlpatterns = [
    path('generate/', generate_students, name='generate'),
    path('', get_students, name='list'),
    path('create/', create_student, name='create'),
    path('edit/<uuid:uuid>', edit_student, name='edit'),
    path('delite/<uuid:uuid>', delete_student, name='delete'),
]
